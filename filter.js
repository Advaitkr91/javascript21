let filter =(elements,cb)=>{
    let res = [] 
    for(let i=0;i<elements.length;i++){
 
         if(cb(elements[i])){

             res.push(elements[i])
         }
   }
   if(res.length == 0){

        return []
   }
   else{

       return res
   }
}
module.exports = filter